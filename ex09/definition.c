#include "header.h"

uint8_t loop_until_bit_is_set(uint8_t reg, uint8_t bit) {
    /**
     * Pour tester, il faut créer un masque en mettant à 0 tous les bits qui n’ont pas d’intérêt pour le test à faire, puis l’utiliser avec l’opérateur bit-à-bit ET.
    */
    uint8_t mask = (1 >> bit);
    while((reg & mask) == 0) {
        //continue;
    }
    return reg; 
}