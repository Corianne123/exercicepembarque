unit8_t a = 0b00001111 ^ 0b11110000; 	resultat: 0b11111111 

unit8_t b = 0b01101100 |= 0b11000011;  resultat: 0b11101111 

unit8_t c = 0b00001010 & 0b11110101; resultat: 0b00000000 

unit8_t d = _BV(7) | _BV(3) | _BV(1); resultat: 0b00000111

unit8_t e = 0b01101001; 
e ^= ~(_BV(4) | _BV(2) | _BV(0));  resultat: 0b10010000
