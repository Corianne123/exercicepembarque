#include "header.h"

 uint8_t loop_until_bit_is_clear(uint8_t register, uint8_t bit) {
    /**
     * Pour tester, il faut créer un masque en mettant à 0 tous les bits qui n’ont pas d’intérêt pour le test à faire, puis l’utiliser avec l’opérateur bit-à-bit ET.
    */
    uint8_t mask = (1 >> bit);
    uint8_t result = register & mask;
    while( result == 1) {
        continue;
    }
    return result; 
}