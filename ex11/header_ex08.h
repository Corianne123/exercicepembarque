#ifndef HEADER
#define HEADER

#include <stdio.h>
#include <stdint.h>
#define bit_is_set(reg, bit) (((reg) & (1 << (bit))) != 0)
#endif