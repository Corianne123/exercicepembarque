#ifndef HEADER
#define HEADER

#include <stdio.h>
#include <stdint.h>


#define loop_until_bit_is_set(reg, bit) \
    do {                            \
        while (!((reg) & (1 << (bit)))) {} \
    } while (0)

#endif