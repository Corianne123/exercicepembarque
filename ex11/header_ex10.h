#ifndef HEADER
#define HEADER

#include <stdio.h>
#include <stdint.h>


#define loop_until_bit_is_clear(reg, bit) \
    do {                            \
        while (((reg) & (1 << (bit)))) {} \
    } while (0)

#endif