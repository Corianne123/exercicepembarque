#include "header.h"

int count_char_in_str(const char target, const char * str) {
    int count = 0;
size_t taille = strlen(str);  
  for (int i = 0; i < taille; i++)
    if (str[i] == target) count++;

  return count;
}