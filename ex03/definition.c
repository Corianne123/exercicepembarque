#include "header.h"

 int is_prime(int n) {
    //les nombres 0 et 1 ne sont pas des nombres premiers
    if(n == 0 || n == 1 || n < 0 ) {
        return n;
    }
    for (int i = 2; i <= n-1; ++i)
    {
        if (n%i == 0) {
            return i; // Si i divise n alors n n'est pas premier
        }
    }
    // si aucun diviseur n'a été trouvé, alors n est un nombre premier
    return 0;

}