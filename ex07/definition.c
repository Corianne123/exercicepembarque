#include "header.h"

int count_me() {
    //Variables statiques sont allouées au début de l'exécution du programme, et ne sont libérées qu'à la fin de l'exécution du programme
    static int count = 0; 
    count++; // Incrémente le compteur à chaque appel
    return count; 
}